#!/usr/bin/env python

from ansible.plugins.inventory import BaseInventoryPlugin, Constructable, Cacheable
import requests

class InventoryModule(BaseInventoryPlugin, Constructable, Cacheable):

    NAME = 'myplugin'

    def parse(self, inventory, loader, path, cache=True):

        # call base method to ensure properties are available for use with other helper methods
        super(InventoryModule, self).parse(inventory, loader, path, cache)

        r = requests.get("http://10.211.55.2/inventory")

        for d in r.json():
            self.inventory.add_host(d["hostname"])
