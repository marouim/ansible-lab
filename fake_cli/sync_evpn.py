from ncclient import manager
import logging
import xml.etree.ElementTree as ET


# logging.basicConfig(
#     level=logging.INFO,
# )

with open('evpn_evi_list_filter.xml', 'r') as f:
    filter = f.read()

with manager.connect(
        host="172.18.248.22",
        port=830,
        username="ansible",
        password="password123",
        hostkey_verify=False,
        look_for_keys=False) \
        as m:

    c = m.get_config(
        source='running',
        filter=filter).data_xml

    with open("config.xml", 'w') as f:
        f.write(c)

    # print(c)

    root = ET.fromstring(c)

    ns = {'l2vpn': 'http://cisco.com/ns/yang/Cisco-IOS-XR-l2vpn-cfg'}

    for evi in root.findall(".//l2vpn:evpnevi", ns):
        print(evi.find('l2vpn:eviid', ns).text)

