from ncclient import manager
import logging
import xml.etree.ElementTree as ET



# logging.basicConfig(
#     level=logging.INFO,
# )

with open('l2vpn_bridge_domain_filter.xml', 'r') as f:
    filter = f.read()

with manager.connect(
        host="172.18.248.22",
        port=830,
        username="ansible",
        password="password123",
        hostkey_verify=False,
        look_for_keys=False) \
        as m:

    c = m.get_config(
        source='running').data_xml

    with open("config_filtered.xml", 'w') as f:
        f.write(c)

    # print(c)

    root = ET.fromstring(c)

    ns = {'l2vpn': 'http://cisco.com/ns/yang/Cisco-IOS-XR-l2vpn-cfg',
          'intf': 'http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg',
          'ethsvc': 'http://cisco.com/ns/yang/Cisco-IOS-XR-l2-eth-infra-cfg'}


    for bg in root.findall(".//l2vpn:bridge-domain-group", ns):
        print(bg.find('l2vpn:name', ns).text)

        for bd in bg.findall('.//l2vpn:bridge-domain', ns):
            print(" . " + bd.find('l2vpn:name', ns).text)

            for circuit in bd.findall('.//l2vpn:bd-attachment-circuit', ns):
                interface_name = circuit.find('l2vpn:name', ns).text
                print(" . . " + interface_name)

                intf = root.find(".//intf:interface-configuration[intf:interface-name='%s']" % interface_name, ns)

                vlan_id = intf.find('.//ethsvc:outer-range1-low', ns)

                print(" . . . Vlan-ID: " + vlan_id.text)

            for bd_evi in bd.findall('.//l2vpn:bridge-domain-evi', ns):
                print(" . . Associated EVI: " + bd_evi.find('.//l2vpn:eviid', ns).text)
