from uuid import uuid1

#!/usr/bin/python
 # Copyright (C) 2018 Bell Canada. All rights reserved.
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #       LICENSE-2.0" target="_blank" rel="nofollow">http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import csv
import sys
import copy
import math
import json
import requests
import urllib
from requests.exceptions import ConnectionError
import uuid
import logging
from enum import Enum
requests.packages.urllib3.disable_warnings()


class RequestType(Enum):
    GET = 1
    DEL = 2
    PUT = 3
    POST = 4

class FailedRequestException(Exception):
    def __init__(self, url, error_code):
        self.message = "Request to URL " + str(url) + " failed with error code " + str(error_code)
    def __init__(self, url, error_code, body):
        self.message = "Request to URL " + str(url) + " failed with error code " + str(error_code) + " and response " + str(body)
    def __str__(self):
        print(self.message)

class ActiveAndAvailableInventoryClient:
    'Client for interacting with ONAP Active and Available Inventory'
    base_url_template = "https://{IP}:{PORT}/aai/v{VERSION}"
    auth_str = "Basic QUFJOkFBSQ=="
    RELATIONSHIP_URL = "/relationship-list/relationship"
    CREATE_L_IFACE_BODY = """
    {
        "interface-name": "{IFACE-NAME}",
        "interface-role": "{IFACE-ROLE}",
        "is-port-mirrored": false,
        "in-maint": false,
        "is-ip-unnumbered": false
    }"""

    CREATE_PSERVER_BODY = """
      {
            "hostname": "{HOSTNAME}",
            "equip-vendor": "{EQUIP-VENDOR}",
            "equip-model": "{EQUIP-MODEL}",
            "serial-number": "{SERIAL-NUMBER}",
            "pserver-id": "{PSERVER-ID}",
            "in-maint": false
        }
    """

    CREATE_PSERVER_P_IFACE_BODY = """
     {
                "interface-name": "{INTERFACE-NAME}",
                "speed-value": "{SPEED-VALUE}",
                "in-maint": false,
                "inv-status": "{INV-STATUS}"
    }"""

    PUT_IFACE_ON_PNF_BODY = """
            {
            "interface-name": "{IFACE-NAME}",
            "interface-role": "{IFACE-ROLE}"
            {RESOURCE-STR}
            }
    """



    RELATIONSHIP_BODY = """
    {
                "related-to": "{RELATED-TO}",
                "related-link": "{RELATED-LINK}",
                "relationship-data": {RELATIONSHIP-DATA}
    }
    """

    PUT_PNF_BODY = """
    {
            "pnf-name": "{PNF-NAME}",
            "pnf-name2": "{PNF-NAME2}",
            "equip-type": "{EQUIP-TYPE}",
            "equip-vendor": "{EQUIP-VENDOR}",
            "equip-model": "{EQUIP-MODEL}",
            "ipaddress-v4-oam": "{IP}",
            "sw-version": "{SW-VERSION}",
            "in-maint": false,
            "frame-id": "{FRAME-ID}",
            "nf-role": "{NF-ROLE}"
    }"""


    PUT_PHYSICAL_LINK_BODY = """
    {
    "link-name": "{LINK-NAME}"
    }"""

    PUT_VLAN_ON_CLOUD_REGION_BODY = """
    {
    "vlan-interface": "{VLAN-INTERFACE}",
    "vlan-id-outer": "{VLAN_OUTER_ID}",
    "in-maint": false,
    "is-ip-unnumbered": false
    }
    """

    PUT_LOGICAL_LINK_BODY = """
        {"link-name": "{LINK-NAME}",
            "in-maint": false,
            "link-type": "{LINK-TYPE}",
            "routing-protocol": "{ROUTING-PROTOCOL}",
            "operational-status": "{OPERATIONAL-STATUS}",
            "link-role": "{LINK-ROLE}",
            "link-name2": "{LINK-NAME2}"}
    """

    GET_PNF_BY_PNF_NAME_2_URL = "/network/pnfs?pnf-name2={PNF-NAME-2}&frame-id={SITE-ID}"
    GET_PNF_URL = "/network/pnfs/pnf/{PNF-NAME}?depth={DEPTH}"
    GET_ALL_PNFS_URL = "/network/pnfs?depth={DEPTH}"
    PUT_PNF_URL = "/network/pnfs/pnf/{PNF-NAME}"
    L_IFACE_LAG_URL = "/network/pnfs/pnf/{PNF-NAME}/lag-interfaces/lag-interface/{LAG-IFACE-NAME-ENCODED}/l-interfaces/l-interface/{L-IFACE-NAME-ENCODED}"
    L_IFACE_PHYS_URL = "/network/pnfs/pnf/{PNF-NAME}/p-interfaces/p-interface/{PHYS-IFACE-NAME-ENCODED}/l-interfaces/l-interface/{L-IFACE-NAME-ENCODED}"
    DELETE_L_IFACE_FROM_LAG_URL = "/network/pnfs/pnf/{PNF-NAME}/lag-interfaces/lag-interface/{LAG-IFACE-NAME-ENCODED}/l-interfaces/l-interface/{L-IFACE-NAME-ENCODED}"
    DELETE_L_IFACE_FROM_PHYS_URL = "/network/pnfs/pnf/{PNF-NAME}/p-interfaces/p-interface/{P-IFACE-NAME-ENCODED}/l-interfaces/l-interface/{L-IFACE-NAME-ENCODED}"
    GET_LAG_INTERFACE_URL = "/network/pnfs/pnf/{PNF-NAME}/lag-interfaces/lag-interface/{LAG-IFACE-NAME-ENCODED}?depth={DEPTH}"
    GET_LOGICAL_LINK_URL = "/network/logical-links/logical-link/{LOGICAL-LINK-ID}?depth={DEPTH}"
    GET_ALL_LOGICAL_LINKS_URL =  "/network/logical-links?depth={DEPTH}"
    PUT_LOGICAL_LINK_URL = "/network/logical-links/logical-link/{LOGICAL-LINK-ID}"
    PUT_CLOUD_REGION_URL = "/cloud-infrastructure/cloud-regions/cloud-region/{CLOUD-OWNER}/{CLOUD-REGION-ID}"
    GET_CLOUD_REGIONS_URL = "/aai/v11/cloud-infrastructure/cloud-regions?cloud-owner={CLOUD-OWNER}&depth={DEPTH}"
    PUT_PSERVER_URL = "/cloud-infrastructure/pservers/pserver/{HOSTNAME}"
    GET_ALL_PSERVERS_URL = "/cloud-infrastructure/pservers?depth={DEPTH}"
    PUT_PSERVER_P_IFACE_URL = "/cloud-infrastructure/pservers/pserver/{HOSTNAME}/p-interfaces/p-interface/{IFACE-NAME}"
    PUT_PIFACE_ON_PNF_URL = "/network/pnfs/pnf/{PNF-NAME}/p-interfaces/p-interface/{IFACE-NAME-ENCODED}"
    GET_ALL_LAG_INTERFACES_URL = "/network/pnfs/pnf/{PNF-NAME}/lag-interfaces?depth={DEPTH}"
    PUT_LAG_IFACE_ON_PNF_URL = "/network/pnfs/pnf/{PNF-NAME}/lag-interfaces/lag-interface/{IFACE-NAME-ENCODED}"
    PUT_PHYSICAL_LINK_URL = "/network/physical-links/physical-link/{LINK-NAME}"
    PUT_VLAN_ON_CLOUD_REGION_URL = "/cloud-infrastructure/cloud-regions/cloud-region/{CLOUD-OWNER}/{CLOUD-REGION-ID}/vlans/vlan/{VLAN-INTERFACE}"
    GET_VLANS_FROM_CLOUD_REGION_URL = "/cloud-infrastructure/cloud-regions/cloud-region/{CLOUD-OWNER}/{CLOUD-REGION-ID}/vlans"
    SERVICE_INSTANCE_URL = "/business/customers/customer/{CUSTOMER-ID}/service-subscriptions/service-subscription/EVPN/service-instances/service-instance/{SERVICE-INSTANCE-ID}"
    def __init__(self, ip, port, api_version = "11", default_depth="0"):
        self.ip = ip
        self.port = port
        self.api_version = api_version
        self.default_depth = default_depth
        self.base_url = ActiveAndAvailableInventoryClient.base_url_template.replace("{IP}", ip).replace("{PORT}", port).replace("{VERSION}", api_version)
        self.headers_standard = {
            "Authorization" : ActiveAndAvailableInventoryClient.auth_str,
            "X-FromAppId" : "PreLoading",
            "Accept" : "application/json",
            "Content-Type" : "application/json"
        }

    def __str__(self):
        return "ActiveAndAvailableInventoryClient for AAI URL: " + self.base_url

    def encode(self, s):
        #return urllib.parse.quote_plus(s)
        return s

    def send_request(self, url, body = "", request_type = RequestType.GET):
        r = None
        if request_type == RequestType.GET:
            r = requests.get(self.base_url + url, data=body, verify=False, headers=self.headers_standard)
        elif request_type == RequestType.DEL:
            r = requests.delete(self.base_url + url, data=body, verify=False, headers=self.headers_standard)
        elif request_type == RequestType.PUT:
            r = requests.put(self.base_url + url, data=body, verify=False, headers=self.headers_standard)
        elif request_type == RequestType.POST:
            r = requests.post(self.base_url + url, data=body, verify=False, headers=self.headers_standard)
        if r is None:
            raise FailedRequestException("Something went wrong sending AAI call: " + url + " with body " + body)
        if int(math.floor(r.status_code / 100)) != 2 :
            raise FailedRequestException(url, r.status_code, r.content)
        return r.content

    def get_service_instance(self, customer_id, service_instance_id):
        url = ActiveAndAvailableInventoryClient.SERVICE_INSTANCE_URL.replace("{CUSTOMER-ID}", customer_id)
        url = url.replace("{SERVICE-INSTANCE-ID}", service_instance_id)
        return json.loads(self.send_request(url))
    def get_all_pnfs(self, depth = None):
        if depth is None:
            depth = self.default_depth
        r = self.send_request(ActiveAndAvailableInventoryClient.GET_ALL_PNFS_URL.replace("{DEPTH}", depth))
        return json.loads(r)["pnf"]

    def get_pnf_by_pnf_name_2(self, site_id, pnf_name2):
        r = self.send_request(ActiveAndAvailableInventoryClient.GET_PNF_BY_PNF_NAME_2_URL.replace("{SITE-ID}", site_id).replace("{PNF-NAME-2}", pnf_name2))
        json_response = json.loads(r)
        return json_response["pnf"][0]

    def get_pnf_by_pnf_name(self, pnf_name, depth=None):
        if depth is None:
            depth = self.default_depth
        r = self.send_request(ActiveAndAvailableInventoryClient.GET_PNF_URL.replace("{PNF-NAME}", pnf_name).replace("{DEPTH}",depth))
        return json.loads(r)

    # Gets a lag interface from a pnf
    def get_lag_interface(self, pnf_name, lag_interface_name, depth=None):
        if depth is None:
            depth=self.default_depth
        lag_iface_encoded = self.encode(lag_interface_name)
        url = ActiveAndAvailableInventoryClient.GET_LAG_INTERFACE_URL.replace("{PNF-NAME}", pnf_name).replace("{LAG-IFACE-NAME-ENCODED}", lag_iface_encoded).replace("{DEPTH}",depth)
        r = self.send_request(url)
        return json.loads(r)

    def get_l_interface_on_lag(self, pnf_name, lag_interface_name, l_interface_name):
        lag_iface_encoded = self.encode(lag_interface_name)
        l_iface_encoded = self.encode(l_interface_name)
        "/network/pnfs/pnf/{PNF-NAME}/lag-interfaces/lag-interface/{LAG-IFACE-NAME-ENCODED}/l-interfaces/l-interface/{L-IFACE-NAME-ENCODED}"
        url = ActiveAndAvailableInventoryClient.DELETE_L_IFACE_FROM_LAG_URL.replace("{PNF-NAME}", pnf_name)
        url = url.replace("{LAG-IFACE-NAME-ENCODED}", lag_interface_name)
        url = url.replace("{L-IFACE-NAME-ENCODED}", l_interface_name)
        return json.loads(self.send_request(url))

    def get_all_lag_interfaces(self, pnf_name, depth=None):
        if depth is None:
            depth = self.default_depth
        url = ActiveAndAvailableInventoryClient.GET_ALL_LAG_INTERFACES_URL.replace("{PNF-NAME}", pnf_name).replace("{DEPTH}", depth)
        r = self.send_request(url)
        return json.loads(r)["lag-interface"]

    def get_logical_link(self, logical_link_id, depth=None):
        if depth is None:
            depth = self.default_depth
        url = ActiveAndAvailableInventoryClient.GET_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", logical_link_id).replace("{DEPTH}", depth)
        r = self.send_request(url)
        return json.loads(r)

    def get_all_logical_links(self, depth=None):
        if depth is None:
            depth = self.default_depth
        url = ActiveAndAvailableInventoryClient.GET_ALL_LOGICAL_LINKS_URL.replace("{DEPTH}", depth)
        r = self.send_request(url)
        return json.loads(r)["logical-link"]

    def delete_pserver(self, hostname):
        url = ActiveAndAvailableInventoryClient.PUT_PSERVER_URL.replace("{HOSTNAME}", hostname)
        r = self.send_request(url, request_type = RequestType.DEL)

    def get_all_pservers(self, depth = None):
        if depth is None:
            depth = self.default_depth
        url = ActiveAndAvailableInventoryClient.GET_ALL_PSERVERS_URL.replace("{DEPTH}", depth)
        r = self.send_request(url)
        return json.loads(r)["pserver"]

    def put_pserver(self, hostname, equip_vendor, equip_model, serial_number, pserver_id):
        url = ActiveAndAvailableInventoryClient.PUT_PSERVER_URL.replace("{HOSTNAME}", hostname)
        body = ActiveAndAvailableInventoryClient.CREATE_PSERVER_BODY.replace("{HOSTNAME}", hostname)
        body = body.replace("{EQUIP-VENDOR}", equip_vendor).replace("{EQUIP-MODEL}", equip_model)
        body = body.replace("{SERIAL-NUMBER}", serial_number).replace("{PSERVER-ID}", pserver_id)
        r = self.send_request(url, body=body, request_type = RequestType.PUT)

    def put_piface_on_pserver(self, hostname, iface_name, speed_value, inv_status):
        url = ActiveAndAvailableInventoryClient.PUT_PSERVER_P_IFACE_URL.replace("{HOSTNAME}", hostname)
        url = url.replace("{IFACE-NAME}", iface_name)
        body = ActiveAndAvailableInventoryClient.CREATE_PSERVER_P_IFACE_BODY.replace("{INTERFACE-NAME}", iface_name)
        body = body.replace("{SPEED-VALUE}", speed_value).replace("{INV-STATUS}", inv_status)
        r = self.send_request(url, body=body, request_type = RequestType.PUT)

    # Adds an l-interface on a lag-interface on a PNF
    def put_l_iface_on_lag(self, pnf_name, lag_name, iface_name, iface_role):
        l_iface_body = ActiveAndAvailableInventoryClient.CREATE_L_IFACE_BODY.replace("{IFACE-NAME}", iface_name)
        l_iface_body = l_iface_body.replace("{IFACE-ROLE}", iface_role)
        l_iface_url = ActiveAndAvailableInventoryClient.L_IFACE_LAG_URL.replace("{LAG-IFACE-NAME-ENCODED}", self.encode(lag_name)).replace("{L-IFACE-NAME-ENCODED}", self.encode(iface_name))
        l_iface_url = l_iface_url.replace("{PNF-NAME}", pnf_name)
        self.send_request(l_iface_url, body=l_iface_body, request_type=RequestType.PUT)

    # Adds an l-interface on a p-interface on a PNF
    def put_l_iface_on_phys(self, pnf_name, phys_name, iface_name, iface_role):
        l_iface_body = ActiveAndAvailableInventoryClient.CREATE_L_IFACE_BODY.replace("{IFACE-NAME}", iface_name)
        l_iface_body = l_iface_body.replace("{IFACE-ROLE}", iface_role)
        l_iface_url = ActiveAndAvailableInventoryClient.L_IFACE_PHYS_URL.replace("{PHYS-IFACE-NAME-ENCODED}", self.encode(phys_name)).replace("{L-IFACE-NAME-ENCODED}", self.encode(iface_name))
        l_iface_url = l_iface_url.replace("{PNF-NAME}", pnf_name)
        self.send_request(l_iface_url, body=l_iface_body, request_type=RequestType.PUT)

    # Adds a relation between 2 objects in AAI
    def put_relation(self, source_url, related_to, related_link, relationship_data):
        relationship_url = source_url + ActiveAndAvailableInventoryClient.RELATIONSHIP_URL
        body = ActiveAndAvailableInventoryClient.RELATIONSHIP_BODY.replace("{RELATED-TO}", related_to)
        body = body.replace("{RELATED-LINK}", related_link)
        body = body.replace("{RELATIONSHIP-DATA}", json.dumps(relationship_data))
        self.send_request(relationship_url, body=body, request_type=RequestType.PUT)

    def put_pnf(self, pnf_name, pnf_name2, equip_type, equip_model, equip_vendor, ip, sw_version, frame_id, nf_role):
        url = ActiveAndAvailableInventoryClient.PUT_PNF_URL.replace("{PNF-NAME}", pnf_name)
        body = ActiveAndAvailableInventoryClient.PUT_PNF_BODY.replace("{PNF-NAME}", pnf_name)
        body = body.replace("{PNF-NAME2}", pnf_name2).replace("{EQUIP-TYPE}", equip_type)
        body = body.replace("{EQUIP-MODEL}", equip_model).replace("{IP}", ip)
        body = body.replace("{SW-VERSION}", sw_version).replace("{FRAME-ID}", frame_id)
        body = body.replace("{EQUIP-VENDOR}", equip_vendor).replace("{NF-ROLE}", nf_role)
        self.send_request(url, body=body, request_type=RequestType.PUT)

    def delete_pnf(self, pnf_name):
        url = ActiveAndAvailableInventoryClient.PUT_PNF_URL.replace("{PNF-NAME}", pnf_name)
        self.send_request(url, request_type=RequestType.DEL)

    def put_piface_on_pnf(self, pnf_name, iface_name, iface_role):
        iface_name_encoded = self.encode(iface_name)
        url = ActiveAndAvailableInventoryClient.PUT_PIFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name)
        url = url.replace("{IFACE-NAME-ENCODED}", iface_name_encoded)
        body = ActiveAndAvailableInventoryClient.PUT_IFACE_ON_PNF_BODY
        body = body.replace("{RESOURCE-STR}", "")
        body = body.replace("{IFACE-NAME}", iface_name).replace("{IFACE-ROLE}", iface_role)
        self.send_request(url, body=body, request_type=RequestType.PUT)

    def put_lag_on_pnf(self, pnf_name, iface_name, iface_role, resource_version=None):
        iface_name_encoded = self.encode(iface_name)
        url = ActiveAndAvailableInventoryClient.PUT_LAG_IFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name)
        url = url.replace("{IFACE-NAME-ENCODED}", iface_name_encoded)
        body = ActiveAndAvailableInventoryClient.PUT_IFACE_ON_PNF_BODY
        if resource_version is None:
            body = body.replace("{RESOURCE-STR}", "")
        else:
            body = body.replace("{RESOURCE-STR}", ', "resource-version" : "'+resource_version+'"')

        body = body.replace("{IFACE-NAME}", iface_name).replace("{IFACE-ROLE}", iface_role)
        self.send_request(url, body=body, request_type=RequestType.PUT)

    def put_physical_link(self, link_name):
        url = ActiveAndAvailableInventoryClient.PUT_PHYSICAL_LINK_URL.replace("{LINK-NAME}", link_name)
        body = ActiveAndAvailableInventoryClient.PUT_PHYSICAL_LINK_BODY.replace("{LINK-NAME}", link_name)
        self.send_request(url, body=body, request_type=RequestType.PUT)

    def put_logical_link(self, link_name, link_name2, link_role, link_type, routing_protocol = "bgp", operational_status = "active"):
        url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", link_name)
        body = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_BODY.replace("{LINK-NAME}", link_name)
        body = body.replace("{LINK-NAME2}", link_name2).replace("{LINK-ROLE}", link_role)
        body = body.replace("{LINK-TYPE}", link_type).replace("{ROUTING-PROTOCOL}", routing_protocol)
        body = body.replace("{OPERATIONAL-STATUS}", operational_status)
        self.send_request(url, body=body, request_type=RequestType.PUT)

    def delete_logical_link(self, link_name):
        url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", link_name)
        self.send_request(url, request_type=RequestType.DEL)

    def get_cloud_regions(self, cloud_owner, depth=None):
        if depth is None:
            depth = self.default_depth
        url = ActiveAndAvailableInventoryClient.GET_CLOUD_REGIONS_URL.replace("{CLOUD-OWNER}", cloud_owner).replace("{DEPTH}", depth)
        r = self.send_request(url)
        json_response = json.loads(r)
        return json_response["cloud-region"]



    def put_vlan_on_cloud_region(self, cloud_owner, cloud_region_id, vlan_interface, vlan_id_outer):
        url = ActiveAndAvailableInventoryClient.PUT_VLAN_ON_CLOUD_REGION_URL
        url = url.replace("{CLOUD-OWNER}", cloud_owner)
        url = url.replace("{CLOUD-REGION-ID}", cloud_region_id)
        url = url.replace("{VLAN-INTERFACE}", vlan_interface)
        body = ActiveAndAvailableInventoryClient.PUT_VLAN_ON_CLOUD_REGION_BODY
        body = body.replace("{VLAN-ID-OUTER}", vlan_id_outer)
        body = body.replace("{VLAN-INTERFACE}", vlan_interface)
        self.send_request(url, body=body, request_type=RequestType.PUT)

    def delete_vlan_from_cloud_region(self, cloud_owner, cloud_region_id, vlan_interface):
        url = ActiveAndAvailableInventoryClient.PUT_VLAN_ON_CLOUD_REGION_URL
        url = url.replace("{CLOUD-OWNER}", cloud_owner)
        url = url.replace("{CLOUD-REGION-ID}", cloud_region_id)
        url = url.replace("{VLAN-INTERFACE}", vlan_interface)
        self.send_request(url, request_type=RequestType.DEL)

    def get_vlan_from_cloud_region(self, cloud_owner, cloud_region_id, vlan_interface):
        url = ActiveAndAvailableInventoryClient.PUT_VLAN_ON_CLOUD_REGION_URL
        url = url.replace("{CLOUD-OWNER}", cloud_owner)
        url = url.replace("{CLOUD-REGION-ID}", cloud_region_id)
        url = url.replace("{VLAN-INTERFACE}", vlan_interface)
        return self.send_request(url)

    def get_vlans_from_cloud_region(self, cloud_owner, cloud_region_id):
        url = ActiveAndAvailableInventoryClient.GET_VLANS_FROM_CLOUD_REGION_URL
        url = url.replace("{CLOUD-OWNER}", cloud_owner)
        url = url.replace("{CLOUD-REGION-ID}", cloud_region_id)
        try:
            return json.loads(self.send_request(url))["vlan"]
        except Exception:
            return None

    def put_relation_phys_link_pnf(self, link_name, pnf_name, iface_name):
        encoded_iface_name = self.encode(iface_name)
        relationship_data = [
            {
                "relationship-key": "pnf.pnf-name",
                "relationship-value": pnf_name
            },
            {
                "relationship-key": "p-interface.interface-name",
                "relationship-value": iface_name
            }
        ]
        link_url = ActiveAndAvailableInventoryClient.PUT_PHYSICAL_LINK_URL.replace("{LINK-NAME}", link_name)
        iface_url = ActiveAndAvailableInventoryClient.PUT_PIFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name).replace("{IFACE-NAME-ENCODED}", encoded_iface_name)
        self.put_relation(link_url, "p-interface", iface_url, relationship_data)

    def put_relation_vlan_to_logical_link(self, cloud_owner, cloud_region_id, vlan_interface, logical_link_name):
        relationship_data = [
            {
                "relationship-key": "logical-link.link-name",
                "relationship-value": logical_link_name
            }
        ]
        link_url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", logical_link_name)
        vlan_url = ActiveAndAvailableInventoryClient.PUT_VLAN_ON_CLOUD_REGION_URL.replace("{CLOUD-OWNER}", cloud_owner)
        vlan_url = vlan_url.replace("{CLOUD-REGION-ID}", cloud_region_id)
        vlan_url = vlan_url.replace("{VLAN-INTERFACE}", vlan_interface)
        self.put_relation(vlan_url, "logical-link", link_url, relationship_data)

    def put_relation_llink_service_instance(self, link_name, customer_id, service_instance_id):
        relationship_data = [
            {
                "relationship-key": "logical-link.link-name",
                "relationship-value": link_name
            }
        ]
        service_instance_url = ActiveAndAvailableInventoryClient.SERVICE_INSTANCE_URL.replace("{CUSTOMER-ID}", customer_id).replace("{SERVICE-INSTANCE-ID}", service_instance_id)
        link_url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", link_name)
        self.put_relation(service_instance_url, "logical-link", link_url, relationship_data)

    def put_relation_llink_llink(self, link_name1, link_name2):
        relationship_data = [
            {
                "relationship-key": "logical-link.link-name",
                "relationship-value": link_name2
            }
        ]
        link_url1 = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", link_name1)
        link_url2 = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", link_name2)
        self.put_relation(link_url1, "logical-link", link_url2, relationship_data)

    def put_relation_phys_link_pserver(self, link_name, hostname, iface_name):
        encoded_iface_name = self.encode(iface_name)
        relationship_data = [
            {
                "relationship-key": "pserver.hostname",
                "relationship-value": hostname
            },
            {
                "relationship-key": "p-interface.interface-name",
                "relationship-value": iface_name
            }
        ]
        link_url = ActiveAndAvailableInventoryClient.PUT_PHYSICAL_LINK_URL.replace("{LINK-NAME}", link_name)
        iface_url = ActiveAndAvailableInventoryClient.PUT_PSERVER_P_IFACE_URL.replace("{HOSTNAME}", hostname).replace("{IFACE-NAME}", encoded_iface_name)
        self.put_relation(link_url, "p-interface", iface_url, relationship_data)

    def delete_physical_link(self, link_name):
        url = ActiveAndAvailableInventoryClient.PUT_PHYSICAL_LINK_URL.replace("{LINK-NAME}", link_name)
        self.send_request(url, request_type=RequestType.DEL)

    def delete_piface_from_pnf(self, pnf_name, iface_name):
        iface_name_encoded = self.encode(iface_name)
        url = ActiveAndAvailableInventoryClient.PUT_PIFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name)
        url = url.replace("{IFACE-NAME-ENCODED}", iface_name_encoded)
        self.send_request(url, request_type=RequestType.DEL)

    # Deletes a relationship between 2 objects in AAI
    def delete_relation(self, source_url, related_to, related_link, relationship_data):
        relationship_url = source_url + ActiveAndAvailableInventoryClient.RELATIONSHIP_URL
        body = ActiveAndAvailableInventoryClient.RELATIONSHIP_BODY.replace("{RELATED-TO}", related_to)
        body = body.replace("{RELATED-LINK}", related_link)
        body = body.replace("{RELATIONSHIP-DATA}", json.dumps(relationship_data))
        self.send_request(relationship_url, body=body, request_type=RequestType.DEL)


    def put_relation_llink_lag_sublinterface(self, link_name, pnf_name, lag_interface_name, subinterface_name):
        relationship_data = [
            {
                "relationship-key": "l-interface.interface-name",
                "relationship-value": subinterface_name
            },
            {
                "relationship-key": "pnf.pnf-name",
                "relationship-value": pnf_name
            }
        ]
        #"/network/pnfs/pnf/{PNF-NAME}/lag-interfaces/lag-interface/{LAG-IFACE-NAME-ENCODED}/l-interfaces/l-interface/{L-IFACE-NAME-ENCODED}"
        logical_link_url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", link_name)
        subinterface_url = ActiveAndAvailableInventoryClient.DELETE_L_IFACE_FROM_LAG_URL.replace("{PNF-NAME}", pnf_name).replace("{LAG-IFACE-NAME-ENCODED}", lag_interface_name)
        subinterface_url = subinterface_url.replace("{L-IFACE-NAME-ENCODED}", subinterface_name)
        self.put_relation(logical_link_url, "l-interface", subinterface_url, relationship_data)

    # Adds a relationship between a logical link and a cloud region
    def put_relation_llink_cloud_region(self, logical_link_id, cloud_owner, cloud_region_id):
        relationship_data = [
            {
                "relationship-key": "cloud-region.cloud-owner",
                "relationship-value": cloud_owner
            },
            {
                "relationship-key": "cloud-region.cloud-region-id",
                "relationship-value": cloud_region_id
            }
        ]
        logical_link_url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", logical_link_id)
        cloud_region_url = ActiveAndAvailableInventoryClient.PUT_CLOUD_REGION_URL.replace("{CLOUD-OWNER}", cloud_owner).replace("{CLOUD-REGION-ID}", cloud_region_id)
        self.put_relation(logical_link_url, "cloud-region", cloud_region_url, relationship_data)

    def put_relation_lag_p_ifaces(self, pnf_name, p_iface_name, lag_iface_name):
        lag_iface_name_encoded = self.encode(lag_iface_name)
        p_iface_name_encoded = self.encode(p_iface_name)
        relationship_data = [
            {
                "relationship-key": "pnf.pnf-name",
                "relationship-value": pnf_name
            },
            {
                "relationship-key": "lag-interface.interface-name",
                "relationship-value": lag_iface_name
            }
        ]
        lag_iface_url = ActiveAndAvailableInventoryClient.PUT_LAG_IFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name).replace("{IFACE-NAME-ENCODED}", lag_iface_name_encoded)
        p_iface_url = ActiveAndAvailableInventoryClient.PUT_PIFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name).replace("{IFACE-NAME-ENCODED}", p_iface_name_encoded)
        self.put_relation(p_iface_url, "lag-interface", lag_iface_url, relationship_data)

    def delete_relation_lag_p_ifaces(self, pnf_name, p_iface_name, lag_iface_name):
        lag_iface_name_encoded = self.encode(lag_iface_name)
        p_iface_name_encoded = self.encode(p_iface_name)
        relationship_data = [
            {
                "relationship-key": "pnf.pnf-name",
                "relationship-value": pnf_name
            },
            {
                "relationship-key": "lag-interface.interface-name",
                "relationship-value": lag_iface_name
            }
        ]
        lag_iface_url = ActiveAndAvailableInventoryClient.PUT_LAG_IFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name).replace("{IFACE-NAME-ENCODED}", lag_iface_name_encoded)
        p_iface_url = ActiveAndAvailableInventoryClient.PUT_PIFACE_ON_PNF_URL.replace("{PNF-NAME}", pnf_name).replace("{IFACE-NAME-ENCODED}", p_iface_name_encoded)
        self.delete_relation(p_iface_url, "lag-interface", lag_iface_url, relationship_data)


    # Deletes a relationship between a logical link and a cloud region
    def delete_relation_llink_cloud_region(self, logical_link_id, cloud_owner, cloud_region_id):
        relationship_data = [
            {
                "relationship-key": "cloud-region.cloud-owner",
                "relationship-value": cloud_owner
            },
            {
                "relationship-key": "cloud-region.cloud-region-id",
                "relationship-value": cloud_region_id
            }
        ]
        logical_link_url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", logical_link_id)
        cloud_region_url = ActiveAndAvailableInventoryClient.PUT_CLOUD_REGION_URL.replace("{CLOUD-OWNER}", cloud_owner).replace("{CLOUD-REGION-ID}", cloud_region_id)
        self.delete_relation(logical_link_url, "cloud-region", cloud_region_url, relationship_data)

    # Puts a relationship between a logical link and a PNF
    def put_relation_llink_pnf(self, logical_link_id, pnf_id):
        relationship_data = [
            {
                "relationship-key": "pnf.pnf-name",
                "relationship-value": pnf_id
            }
        ]
        logical_link_url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", logical_link_id)
        pnf_url = ActiveAndAvailableInventoryClient.PUT_PNF_URL.replace("{PNF-NAME}", pnf_id)
        self.put_relation(logical_link_url, "pnf", pnf_url, relationship_data)

    # Deletes a relationship between a logical link and a PNF
    def delete_relation_llink_pnf(self, logical_link_id, pnf_id):
        relationship_data = [
            {
                "relationship-key": "pnf.pnf-name",
                "relationship-value": pnf_id
            }
        ]
        logical_link_url = ActiveAndAvailableInventoryClient.PUT_LOGICAL_LINK_URL.replace("{LOGICAL-LINK-ID}", logical_link_id)
        pnf_url = ActiveAndAvailableInventoryClient.PUT_PNF_URL.replace("{PNF-NAME}", pnf_id)
        self.delete_relation(logical_link_url, "pnf", pnf_url, relationship_data)

    # Deletes an l-interface from a lag interface on a PNF
    def delete_l_iface_on_lag(self, pnf_name, lag_iface_name, l_iface_name):
        lag_iface_encoded = self.encode(lag_iface_name)
        l_iface_name_encoded = self.encode(l_iface_name)
        url_to_delete = ActiveAndAvailableInventoryClient.DELETE_L_IFACE_FROM_LAG_URL.replace("{PNF-NAME}", pnf_name)
        url_to_delete = url_to_delete.replace("{LAG-IFACE-NAME-ENCODED}", lag_iface_encoded).replace("{L-IFACE-NAME-ENCODED}", l_iface_name_encoded)
        self.send_request(url_to_delete, request_type = RequestType.DEL)

    # Deletes an l-interface from a p-interface on a PNF
    def delete_l_iface_on_phys(self, pnf_name, p_iface_name, l_iface_name):
        p_iface_encoded = self.encode(p_iface_name)
        l_iface_name_encoded = self.encode(l_iface_name)
        url_to_delete = ActiveAndAvailableInventoryClient.DELETE_L_IFACE_FROM_PHYS_URL.replace("{PNF-NAME}", pnf_name)
        url_to_delete = url_to_delete.replace("{P-IFACE-NAME-ENCODED}", p_iface_encoded).replace("{L-IFACE-NAME-ENCODED}", l_iface_name_encoded)
        self.send_request(url_to_delete, request_type = RequestType.DEL)

    # Gets all relationships for an AAI object that have this value as the related-to field
    def get_relationships_by_related_to(self, obj, related_to):
        if obj is None or "relationship-list" not in obj.keys() or "relationship" not in obj["relationship-list"]:
            return None
        relationships = obj["relationship-list"]["relationship"]
        return filter(lambda r: r["related-to"] == related_to, relationships)

    def get_relationship_datum(self, relationship, relationship_key):
        if relationship is None or relationship["relationship-data"] is None:
            return None
        relationship_data = relationship["relationship-data"]
        relationship_data_filtered = list(filter(lambda r : r["relationship-key"] == relationship_key, relationship_data))
        if len(relationship_data_filtered) < 1:
            return None
        return relationship_data_filtered[0]["relationship-value"]

    # Fetches all related objects with matching related-to value
    def get_objs_by_related_to(self, obj, related_to):
        relationships = self.get_relationships_by_related_to(obj, related_to)
        return map(lambda r : json.loads(self.send_request(r["related-link"])), relationships)

    def get_all_pnfs_in_tor_group(self, tor_group_name):
        pnfs = self.get_all_pnfs()
        pnfs_in_tg = []
        for pnf in pnfs:
            pnf_name = pnf["pnf-name"]
            if len(filter(lambda iface : iface["interface-role"] == tor_group_name, self.get_all_lag_interfaces(pnf_name))) > 0:
                pnfs_in_tg.append(pnf)
        return pnfs_in_tg



    def get_vlan_info_for_cloud_region(self, cloud_owner, cloud_region_id, known_vlans=[]):
        site_id = cloud_region_id.split("-")[0]
        ecmp_disabled = "openstack" in cloud_region_id.lower() or "vmware" in cloud_region_id.lower()
        vlans = self.get_vlans_from_cloud_region(cloud_owner, cloud_region_id)
        vlans = filter(lambda vlan: vlan is not None and "vlan-id-outer" in vlan.keys() and "relationship-list" in vlan.keys(), vlans)
        vlan_map = {}
        for vlan in vlans:
            vlan_id = vlan["vlan-id-outer"]
            if vlan_id not in known_vlans:
                evi_info = self.get_all_vlan_info(vlan, site_id)
                evi_info["evi_ecmp"] = ecmp_disabled
                vlan_map[vlan_id] = evi_info
        return vlan_map

    def get_all_vlan_info(self, vlan, site_id):
        related_evi = self.get_objs_by_related_to(vlan, "logical-link")[0]
        evi_id = related_evi["link-name2"]
        bd = self.get_objs_by_related_to(related_evi, "logical-link")[0]
        bd_name = bd["link-name2"]
        evi_route_target = site_id + ":" + evi_id

        bvis = self.get_objs_by_related_to(bd, "bvi")
        bvi = None
        if len(bvis) > 0:
            bvi = bvis[0]
        bvi_info = None
        if bvi is not None:
            bvi_info = {'name': bvi["interface-name"],
                        'ip': bvi["ipv4-oam-address"],
                        'vrf': self.get_relationship_datum(self.get_relationships_by_related_to(bvi, "vrf")[0],
                                                           "vrf.vrf-name"),
            }

        evi_info = {'bg_name': bd_name,
                  'bd_name': "bg_tenant",
                  'evi_id': evi_id,
                  'evi_rt': evi_route_target,
                  'bvi': bvi_info
                  }
        return evi_info

def get_nso_vlans():
    all_cloud_regions = nso_aai.get_cloud_regions("Bell-IaaS")
    vlan_dict = {}
    for cloud_region in all_cloud_regions:
        cloud_region_id = cloud_region["cloud-region-id"]

aai = ActiveAndAvailableInventoryClient("10.195.177.122", "31420")


class ActiveAndAvailableInventoryException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

def add_iface_to_aai(bd_link_name, pnf_name, lag_interface_name, l_interface_name):
    existing_l_iface = None
    # If the interface already exists, do nothing
    # otherwise add it
    try:
        existing_l_iface = aai.get_l_interface_on_lag(pnf_name, lag_interface_name, l_interface_name)
    except Exception:
        aai.put_l_iface_on_lag(pnf_name, lag_interface_name, l_interface_name, "")
    aai.put_relation_llink_lag_sublinterface(bd_link_name, pnf_name, lag_interface_name, l_interface_name)

    if existing_l_iface:
        return

def push_evpn_config(customer_id, service_instance_id, evi_info):
    evi_link_name = str(uuid1())
    bd_link_name = str(uuid1())
    try:
        evi_id = evi_info["evi_id"]
        evi_name = evi_info["evi_name"]
        bd_name = evi_info["bd_name"]
        ifaces = evi_info["ifaces"]
    except KeyError as e:
        raise ActiveAndAvailableInventoryException("Failed to provide evi_info key:"+ str(e))
    # Create EVI in AAI
    aai.put_logical_link(evi_link_name, evi_id, evi_name, "evi")
    # Create Bridge Domain
    aai.put_logical_link(bd_link_name, bd_name, "bridge-domainTEST_EVI", "bridge-domain")
    # Add relationship from EVI to service instance
    aai.put_relation_llink_service_instance(evi_link_name, customer_id, service_instance_id)
    aai.put_relation_llink_llink(evi_link_name, bd_link_name)

    #Add relationships between each subinterface and the bridge domain
    for iface in ifaces:

        try:
            pnf_name = iface["pnf_name"]
            lag_interface_name = iface["lag_interface_name"]
            l_interface_name = iface["l_interface_name"]
        except KeyError as e:
            raise ActiveAndAvailableInventoryException("Failed to provide interface information key:" + str(e))

        add_iface_to_aai(bd_link_name, pnf_name, lag_interface_name, l_interface_name)

    return evi_link_name

def delete_evpn_config(link_name):
    evi = aai.get_logical_link(link_name)
    bds = aai.get_objs_by_related_to(evi, "logical-link")
    for bd in bds:
        l_iface_relationships = aai.get_relationships_by_related_to(bd, "l-interface")
        for l_iface_rel in l_iface_relationships:
            pnf_name = aai.get_relationship_datum(l_iface_rel, "pnf.pnf-name")
            lag_iface = aai.get_relationship_datum(l_iface_rel, "lag-interface.interface-name")
            l_iface_name = aai.get_relationship_datum(l_iface_rel, "l-interface.interface-name")
            aai.delete_l_iface_on_lag(pnf_name, lag_iface, l_iface_name)
        aai.delete_logical_link(bd["link-name"])
    aai.delete_logical_link(link_name)

def fix_interfaces(yaml_iface_list, vlan_id):
    ifaces = []
    for tor_name in yaml_iface_list.keys():
        if not isinstance(yaml_iface_list[tor_name]["interfaces"], list):
            list_version = []
            list_version.append(yaml_iface_list[tor_name]["interfaces"])
            yaml_iface_list[tor_name]["interfaces"] = list_version
        for iface_name in yaml_iface_list[tor_name]["interfaces"]:
            ifaces.append({
                "pnf_name" : yaml_iface_list[tor_name]["aai_id"],
                "lag_interface_name" : iface_name,
                "l_interface_name" : iface_name + "." + vlan_id
            })
    return ifaces

def get_evi_link_name(service_instance_id, customer_id, evi_id):
    service_instance = aai.get_service_instance(customer_id, service_instance_id)
    logical_links = aai.get_objs_by_related_to(service_instance, "logical-link")
    for logical_link in logical_links:
        if logical_link["link-name2"] == evi_id:
            return logical_link["link-name"]
    raise Exception("Could not find evi in given service instance")

from ansible.module_utils.basic import AnsibleModule
def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        cmd=dict(type='str', required=True),
        evi_info=dict(type="dict", required=False),
        service_instance_id = dict(type="str", required=False),
        customer_id = dict(type="str", required=False),
        evi_id=dict(type="str", required=False)

    )
    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )
    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )
    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        return result
    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    # result['topology'] = module.params['topology']
    # result['vm_list'] = module.params['vm_list']

    if module.params["cmd"] == "create":
        evi_info = module.params["evi_info"]
        if "vlan_id" not in evi_info.keys():
            raise Exception("Missing vlan id")
        evi_info["ifaces"] = fix_interfaces(evi_info["interfaces"], evi_info["vlan_id"])
        service_instance_id = module.params["service_instance_id"]
        customer_id = module.params["customer_id"]
        generated_evi_name = push_evpn_config(customer_id, service_instance_id, evi_info)
        result["message"] = generated_evi_name
        result["changed"] = True


    elif module.params["cmd"] == "delete":
        service_instance_id = module.params["service_instance_id"]
        customer_id = module.params["customer_id"]
        evi_link_name = get_evi_link_name(service_instance_id, customer_id, module.params["evi_id"])
        delete_evpn_config(evi_link_name)
        result["message"] = "Success"
        result["changed"] = True

    else:
        raise Exception("Command not found!")

    result['my_result'] = "Done!"
    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    # if module.params['new']:
    #     result['changed'] = True
    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    # Fail condition
    # if module.params['name'] == 'fail me':
    #     module.fail_json(msg='You requested this to fail', **result)
    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()
if __name__ == '__main__':
    main()
