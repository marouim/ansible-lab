from flask import Flask, jsonify, request, abort
import json
import uuid

DATA = 'service_instances.json'
INVENTORY = 'inventory.json'

app = Flask(__name__)


@app.route("/inventory", methods=['GET'])
def get_inventory():
    with open(INVENTORY, 'r') as f:
        d = json.load(f)

    return jsonify(d['inventory'])


@app.route("/service-instances", methods=['GET'])
def get_service_instances():
    with open(DATA, 'r') as f:
        d = json.load(f)

    return jsonify(d['service_instances'])


@app.route("/service-instances/<instance_id>", methods=['GET'])
def get_service_instance(instance_id):
    with open(DATA, 'r') as f:
        d = json.load(f)

    for i in d['service_instances']:
        if i['id'] == instance_id:
            return jsonify(i)

    abort(404)


@app.route("/service-instances/<instance_id>", methods=['DELETE'])
def delete_service_instance(instance_id):
    with open(DATA, 'r') as f:
        d = json.load(f)

    found = False
    new_data = {
        "service_instances": []
    }

    for i in d['service_instances']:
        if i['id'] == instance_id:
            found = True
        else:
            new_data['service_instances'].append(i)

    if found:
        with open(DATA, 'w') as f:
            json.dump(new_data, f)

        return "", 204
    else:
        abort(404)


@app.route("/service-instances", methods=['POST'])
def create_service_instance():

    payload = request.get_json()
    payload['id'] = str(uuid.uuid4())[:5]

    with open(DATA, 'r') as f:
        d = json.load(f)

    d['service_instances'].append(payload)

    with open(DATA, 'w') as f:
        json.dump(d, f)

    return jsonify(payload)


app.run(debug=True, host='0.0.0.0', port=5001)
