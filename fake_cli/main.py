
cmd = ''
prompt = "nso#"
config_mode = False
service_mode = False
service_cmd = ["evi_id", "vlan_id"]
service_config = {}


def display_service_help():
    print("evi_id:          ID of the EVI")
    print("vlan_id:         VLAN ID for sub-interfaces")


def launch_playbook():
    print("Launching playbook... ")
    print("")


while cmd != 'exit':
    cmd = input(prompt)

    if cmd == "config t":
        prompt = "nso(config)#"
        config_mode = True

    elif cmd == "exit":
        if service_mode:
            service_mode = False
            service_name = ""
            prompt = "nso(config)#"
            cmd = ""
        elif config_mode:
            config_mode = False
            prompt = "nso#"
            cmd = ""

    elif "service" in cmd and config_mode:
        service_name = cmd.split(' ')[1]
        service_mode = True
        prompt = "nso(config-service-%s)#" % service_name

    elif "?"in cmd and service_mode:
        display_service_help()

    elif cmd.split(' ')[0] in service_cmd and service_mode:
        service_config[cmd.split(' ')[0]] = cmd.split(' ')[1]

    elif "show" in cmd and service_mode:
        print(service_config)

    elif "commit" in cmd and service_mode:
        launch_playbook()