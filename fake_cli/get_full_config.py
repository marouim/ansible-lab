from ncclient import manager
import logging
import xml.etree.ElementTree as ET


# logging.basicConfig(
#     level=logging.INFO,
# )

with manager.connect(
        host="172.18.248.22",
        port=830,
        username="ansible",
        password="password123",
        hostkey_verify=False,
        look_for_keys=False) \
        as m:

    c = m.get_config(
        source='running').data_xml

    with open("config.xml", 'w') as f:
        f.write(c)
